#!/bin/bash

echo "[INFO] UPDATE SYSTEM"
sudo apt-get update
echo "[INFO] INSTALL PYTHON 3"
sudo apt-get install python3-pip
sudo -H pip3 install --upgrade pip
sudo apt-get install unzip
pip install kaggle --user
sudo ln -s ~/.local/bin/kaggle /usr/bin/kaggle
export KAGGLE_USERNAME=levanpon1009
export KAGGLE_KEY=e8b853e7d6e3f3de1746a240289ba4c2

# install cuda 10
echo "[INFO] INSTALL CUDA 10.0"
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
sudo apt-get update
sudo apt-get -y install cuda-10.0

#install cudnn
echo "[INFO] INSTALL CUDNN For CUDA 10.0"
sudo dpkg -i libcudnn7_7.6.5.32-1+cuda10.0_amd64.deb
sudo dpkg -i libcudnn7-dev_7.6.5.32-1+cuda10.0_amd64.deb

echo "[INFO] REBOOT"
sudo reboot 
